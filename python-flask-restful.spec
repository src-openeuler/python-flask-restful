Name:           python-flask-restful
Version:        0.3.10
Release:        2
Summary:        Framework for creating REST APIs
License:        BSD-3-Clause
URL:            https://www.github.com/flask-restful/flask-restful/
Source0:       	https://files.pythonhosted.org/packages/c0/ce/a0a133db616ea47f78a41e15c4c68b9f08cab3df31eb960f61899200a119/Flask-RESTful-0.3.10.tar.gz 

BuildArch:      noarch
BuildRequires:  git gcc python3-pytz python3-setuptools python3-mock python3-blinker
BuildRequires:  python3-flask python3-six python3-aniso8601 python3-pytz python3-devel

%description
Flask-RESTful provides the building blocks for creating a REST API.

%package -n python3-flask-restful
Summary:        Framework for creating REST APIs
Requires:       python3-flask python3-six python3-aniso8601 python3-pytz
%{?python_provide:%python_provide python3-flask-restful}

%description -n python3-flask-restful
Flask-RESTful is Python 3 extension for Flask that adds support
for quickly building REST APIs. 

%prep
%autosetup -n Flask-RESTful-%{version} -p1
rm -rf docs/_themes/.gitignore

%build
%py3_build

%install
%py3_install

%check
#%{__python3} setup.py test

%files -n python3-flask-restful
%doc AUTHORS.md README.md examples/ docs/
%license LICENSE
%{python3_sitelib}/*

%changelog
* Mon Aug 14 2023 xu_ping <707078654@qq.com> - 0.3.10-2
- Remove dependencies python3-crypto only used during the test.

* Tue Jul 04 2023 chenzixuan <chenzixuan@kylinos.cn> - 0.3.10-1
- Update package to version 0.3.10

* Fri Jun 24 2022 houyingchao <houyingchao@h-partners.com> - 0.3.9-1
- Upgrade to 0.3.9

* Fri Jun 17 2022 liukuo <liukuo@kylinos.cn> - 0.3.8-4
- License compliance rectification

* Sat Jan 08 2022 shixuantong <shixuantong@huawei.com> - 0.3.8-3
- remove python3-nose buildrequire and disable %check

* Thu Feb 18 2021 shixuantong <shixuantong@huawei.com> - 0.3.8-2
- Fix testsuite for werkzeug 1.x

* Tue Feb 2 2021 liudabo <liudabo1@huawei.com> - 0.3.8-1
- upgrade version to 0.3.8

* Thu Oct 29 2020 tianwei <tianwei12@huawei.com> - 0.3.6-11
- delete python2

* Tue Feb 18 2020 Buildteam <buildteam@openeuler.org> - 0.3.6-10
- Fixbug in patching

* Fri Nov 8 2019 Buildteam <buildteam@openeuler.org> - 0.3.6-9
- Package Initialization
